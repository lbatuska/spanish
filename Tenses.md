# The Indicative Mood (El Modo Indicativo)

We use the indicative mood **in order to talk about true events, facts, actions and states**.

## Presente Simple (Present Simple)

Generally speaking, we use the presente simple in Spanish as we use it in English, i.e. primarily to talk about **things that are true in general, things that are true at the moment of speaking, repeated actions, habits and descriptions**

However, there are a few instances when Spanish would use the present simple while English would use a different tense

a) When talking about **actions that are happening at present but not necessarily at the moment of speaking**

b) When talking about **future plans**

c) When **something started happening in the past and it’s still going on**, especially with words such as desde, desde que and desde hace and the verbal periphrasis llevar + gerund

## Pretérito Imperfecto (Imperfect)

We use the Spanish imperfecto in order to talk about **habitual past actions, things that were taking place in the past when something else happened and descriptions in the past**. A good rule of thumb here is to remember that if you would say “used to” or use the past continuous in English, you’ll generally have to use the imperfect in Spanish

Spanish also uses the construction estar + gerundio, which is very common in this kind of situation.
(Juana was cooking when Pedro came back.) :

> Juana cocinaba cuando Pedro volvió. -> Juana estaba cocinando cuando Pedro volvió.

## Pretérito Indefinido (Preterite)

We use the Spanish preterite in order to talk about **actions that both started and finished in the past**.

Similarly, we use the preterite to discuss **actions that took place a number of times in the past during a certain time period** or **actions that interrupted another ongoing action in the past**.

Finally, we use the preterite for **actions that are part of a chain of past events**.

## Futuro Simple (Future Simple)

We use the future simple in both Spanish and English in a very similar way.

For starters, we use it to **describe actions that will happen in the future, predictions and forecasts**

We also use it for **The 10 Commandments and other types of so-called solemn commands**. You use will/shall in English, so we’re still on the same page

However, there’s one other use of the future simple in Spanish that drives some learners crazy. We also use it to talk about **present possibilities and conjectures**

## Condicional Simple (Simple Conditional)

We use the Spanish conditional to talk about **hypothetical situations, make polite requests, express wishes, make assumptions about the past, give advice and express frustration or regret**. Quite a wide range of possibilities!

## Pretérito Perfecto (Present Perfect)

Even though Spanish calls this tense pretérito (past), and English calls it present, this tense is actually quite similar in both languages:

Use the Spanish pretérito perfecto when you want to talk about **actions that have been recently completed** and **actions that started in the past but are still ongoing**. Beyond that, this tense is used to describe **life experiences (or lack thereof) and completed actions that have an impact on your future**

## Pretérito Pluscuamperfecto (Past Perfect, a.k.a. Pluperfect)

Both the pluscuamperfecto and the English past perfect are used for the same two purposes: to talk about **an action that happened before another past action**, and to talk about **life experiences we’re having for the first time in our lives**

## Pretérito Anterior (Past Anterior or Preterite Perfect)

As much as I love this tense, the pretérito anterior is very rarely used in Spanish nowadays (except for its appearance in literature and very formal language). It has been replaced almost completely by the pluscuamperfecto, and this shouldn’t come as a surprise if we take into account the past anterior is only used to talk about **a past action that took place right before another past action**

## Futuro Compuesto (Future Perfect)

We use the futuro compuesto to talk about **actions that will have been completed in the future and to make conjectures and hypotheses about the past** (much like the simple future is used to make conjectures about the present)

## Condicional Compuesto (Conditional Perfect)

We use the Spanish condicional compuesto to talk about **possibility or impossibility in the past** (things that would or could have happened or not) and to **make wishes and suppositions about the past**

# The Subjunctive Mood (El Modo Subjuntivo)

We use the Spanish subjunctive in order to **talk about wishes, emotions, doubts, abstract things and unknown things**.

## Presente del Subjuntivo (Present Subjunctive)

We use the present subjunctive in order to **express wishes, emotions, doubts, purposes and hopes**, all of them related to the present or the future

## Imperfecto del Subjuntivo (Imperfect Subjunctive)

We use the imperfecto del subjuntivo to talk about **past experiences and our present opinion on past events, to express wishes and doubt and to make very polite requests**

## Futuro del Subjuntivo (Future Subjunctive)

Much as is happening with the pretérito anterior, the futuro del subjuntivo is becoming obsolete and is mainly used in literature and legal documents. On a daily basis, it has been replaced almost completely by the present subjunctive and the present indicative.

We use the futuro del subjuntivo **when the main verb requires the subjunctive and it refers to the future**

## Pretérito Perfecto del Subjuntivo (Present Perfect Subjunctive)

Similarly to the present subjunctive, we use the present perfect subjunctive in order to **express wishes, emotions, doubts, purposes and hopes (or regrets)**. But this time, we’re **referring to an already completed action or an action that will be completed in the future** (much like the future perfect does in English)

## Pluscuamperfecto del Subjuntivo (Pluperfect Subjunctive)

We use the Spanish pluscuamperfecto del subjuntivo to talk about **past events contrary to reality (third conditional) and to wish something had or hadn’t happened in the past**

## Futuro Compuesto del Subjuntivo (Future Perfect Subjunctive)

The Spanish futuro compuesto del subjuntivo is, as its simple counterpart, absolutely obsolete. It’s only present in very formal literature, poetry and legal texts, and it’s used to refer to **a future completed event that would only be true if the condition of an earlier event is fulfilled**. Does it sound awful? Well, it is, even for us native Spanish speakers!

This mouthful simply means that something can be completed in the future if and only if a prior condition is fulfilled. In other words, **something has to happen first, and then a second event will take place and be completed**.

# The Imperative Mood (El Modo Imperativo)

Spanish imperative is used for the same purposes as in English: for **requests, commands, invitations, suggestions, asking for and giving permission, making wishes and apologizing**.

Just as in English, the Spanish imperative can be affirmative and negative. Watch out, though. The Spanish imperative is conjugated, and it has different conjugations for its affirmative and negative forms!

## Imperativo Afirmativo (Affirmative Imperative)

## Imperativo Negativo (Negative Imperative)

source: https://www.fluentu.com/blog/spanish/spanish-tenses/
